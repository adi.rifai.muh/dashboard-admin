import React from 'react';
import { Delete, Admin, Resource } from 'admin-on-rest';
import { PostList } from './component/PostList';
import { PostEdit } from './component/PostEdit';
import { PostCreate } from './component/PostCreate';
import { UserList } from './component/UserList';
import authClient from './client/authClient';
import myApiRestClient from './client/restClient';
import Dashboard from './dashboard';

import PostIcon from 'material-ui/svg-icons/action/book';
import UserIcon from 'material-ui/svg-icons/social/group';

const App = () => (

  <Admin authClient={authClient} restClient={myApiRestClient} dashboard={Dashboard}>
    <Resource name="posts" list={PostList} edit={PostEdit} create={PostCreate} remove={Delete} icon={PostIcon} />
    <Resource name="admin" list={UserList} icon={UserIcon} />
  </Admin>
);

export default App;