import React from 'react';
import {
    List,
    Filter,
    SimpleList,
    Responsive,
    Datagrid,
    ReferenceField,
    TextField,
    EditButton,
    ReferenceInput,
    SelectInput,
    TextInput
}
    from 'admin-on-rest';


export const PostList = (props) => {
    console.log(props)
    return (
        <List {...props} filters={<PostFilter />}>
            <Responsive
                small={
                    <SimpleList
                        primaryText={record => record.title}
                        secondaryText={record => `${record.views} views`}
                        tertiaryText={record => new Date(record.published_at).toLocaleDateString()}
                    />
                }
                medium={
                    <Datagrid>
                        <TextField source="id" />
                        <TextField source="title" />
                        <TextField source="content" />
                        <EditButton />
                    </Datagrid>
                }
            />
        </List>
    )
}
const PostFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <ReferenceInput label="User" source="userId" reference="users">
            <SelectInput optionText="name" />
        </ReferenceInput>
    </Filter>
);
