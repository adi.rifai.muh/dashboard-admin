import React from 'react';
import { Create,
         LongTextInput,
         ReferenceInput,
         required,
         SelectInput,
         SimpleForm,
         TextInput }
from 'admin-on-rest';
    
export const PostCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <ReferenceInput label="User" source="userId" reference="users" validate={required} allowEmpty>
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="title" />
            <LongTextInput source="body" />
        </SimpleForm>
    </Create>
);